#!/usr/bin/perl -w

##might not need to include the start

#debugging messages have 3 \t for indentation to see them more easily
$debugging = 0;

$file = $ARGV[0];
open(FILE, $file) or die("Could not open file. $!");
open(SECOND_FILE,'>', "second_pass.txt") or die ("Could not open second file. $!");
$linecount = 0;
@variableList = ();
@lineAlteredFirstPass = ();

while ($line = <FILE>) {
	$lineAlteredFirstPass[$linecount] = 1;
	#the first line of the program with the language directive
	if ($linecount == 0 and $line =~ /^#!.*/) {
		$line = "#!/usr/bin/perl -w\n";
	} else {
		if ($line =~ /^\s*#/ or $line =~ /^\s*$/) {
			#leave comments and whitespace lines alone
		} 
		elsif ($line =~ /^\s*print\s*"(.*)"\s*$/) { #print with text
			#pythons print adds a new line by default, so we must modify this behaviour
			$line =~ /^\s*print\s*"(.*)"/;
			chomp($line);
			$line .= ", \"\\n\";\n";
		}
		elsif ($line =~ /^\s*print\s*.*\s*$/) { #print with variables
			chomp($line);
			$line .= ", \"\\n\";\n";
		}
		elsif ($line =~ /^\s*break\s*$/) { #substitute break with last
			$line =~ s/break/last\;/;
		}
		elsif ($line =~ /^\s*continue\s*$/) { #substitute continue with next
			$line =~ s/continue/next\;/;
		}
		elsif ($line =~ /^\s*(if|while)\s*([^:]*):\s\S+/) { #if, while on the one line
			chomp($line);
			$match1 = $2;
			$line =~ s/$match1/\($match1\)/;
			$line =~ s/:(.*)/\ {\n\t/; #indentation could do with work later
			print(SECOND_FILE "$line"); 

			$linecount++;
			$lineAlteredFirstPass[$linecount] = 1; #could have a while loop here with the $1 variable to split up multiple statements on one line
			print(SECOND_FILE "$1");

			$linecount++;
			$lineAlteredFirstPass[$linecount] = 1;
			$line = ";\n";
			print(SECOND_FILE "$line");

			$line = "}\n";
		}
		#previous regex which didn't allow for variables (non digits): /^\s*(for)\s+([a-zA-Z_][a-zA-Z\d_]*)\s+(in\s+range\s*\(\s*(\d*)\s*\,\s*(\d*)\s*\)\s*):/
		elsif ($line =~ /^\s*(for)\s+([a-zA-Z_][a-zA-Z\d_]*)\s+(in\s+range\s*\(\s*(.*)\s*\,\s*(.*)\s*\)\s*):/) { #for var in range (x, y):
			if ($debugging == 1) {
				printf("For loop detected. Variable: '%s'\n", "$1");
			}
			$match1 = $1;
			$match2 = $2;
			$match3 = quotemeta($3); #()'s are in the match
				#print($match3, "\n\n");
			$match4 = $4;
			$match5 = $5." - 1"; #python does not include the final value in the interval, perl does
			$line =~ s/$match1/foreach/;
			$line =~ s/$match2/\$$match2/;
			$line =~ s/$match3/\($match4..$match5\)/; #have not yet finished wrapping this, as we have not taken into account any indentation sysetm
			$line =~ s/:\s*$/\ {\n/;
		}
		elsif ($line =~ /\s*(range\s*\(\s*(.*)\s*\,\s*(.*)\s*\))\s*/) { #range by itself
			$match2 = $2;
			$match3 = $3." - 1"; #python does not include the final value in the interval, perl does
			$line =~ s/$match1/\($match2..$match3\)/;
		}
		#all other functions here
		else {
			#possibly turn this line into a comment at the final pass
			#if we could not translate it
			$lineAlteredFirstPass[$linecount] = 0;
		}


		if ($line =~ /^\s*([a-zA-Z_][a-zA-Z\d_]*)\s*=\s*/) { #variables
			#get tighter grip on variable naming, including digits and underscores			
			chomp($line);
			$newVarName = $1;
			$duplicated = 0; #if we already have the variable in the array, don't add it again
			foreach $x (@variableList) {
				if ($x eq $newVarName) {
					$duplicated = 1;
					last;
				}
			}
			if ($duplicated == 0) {
				push(@variableList, $newVarName);		
				if ($debugging == 1) {
					printf("\t\t\tVariable determined: '%s'\n", $newVarName);
				}
			} else {
				if ($debugging == 1) {
					printf("\t\t\tVariable disregarded for duplication: '%s'\n", $newVarName);
				}
			}
			#$line =~ /[^\s].*/;
			$line = $line.";\n";
			$lineAlteredFirstPass[$linecount] = 1;
		}
	}
	print(SECOND_FILE "$line");
	$linecount++;
}
close(FILE);
close(SECOND_FILE);

#printf("~~~~~~~~~~~~~~~Second pass starting.\n");

#second pass
$linecount = 0;
@lineAlteredSecondPass = ();
open(FILE, "second_pass.txt") or die("Could not open file. $!");
while ($line = <FILE>) {
	#subsititutions of variables, append $ to the beginning
	$lineAlteredSecondPass[$linecount] = 0;
	foreach $variable (@variableList) {
		#this loop discounts the cases of the variable at the beginning of the line [and the end of the line - actually it does handle this case]
		while ($line =~ /([^a-zA-Z_\d]*[^a-zA-Z_\d\$])$variable([^a-zA-Z_\d]+)/) {
			$match1 = ($1); #quotemeta maybe
			$match2 = ($2);
			$line =~ s/[^a-zA-Z_\d]*[^a-zA-Z_\d\$]$variable[^a-zA-Z_\d]+/$match1\$$variable$match2/;
			if ($debugging == 1) {
				printf("\t\t\tVariable matched!! Last match: '%s'\n", $&);
			}
			$lineAlteredSecondPass[$linecount] = 1;
		}
		#handle the beginning of the line case;
		#note we are assuming there were never be a line with just:
		#variable
		#on it, and nothing else
		if ($line =~ /^$variable[^a-zA-Z_\d]+/) {
			$line = "\$".$line;
		}
		#handle the end of line case; similar assumptions to the above;
		#seems to be handled in the above, but keep this to uncomment just in case for future reference
				#if ($line =~ /([^a-zA-Z_\d]*[^a-zA-Z_\d\$])$variable$/) {
				#	$match1 = $1;
				#	$line =~ s/[^a-zA-Z_\d]*[^a-zA-Z_\d\$]$variable$/$match1\$$variable/;
				#}
	}
	if ($lineAlteredFirstPass[$linecount] == 0 and $lineAlteredSecondPass[$linecount] == 0) {
		$line = "###".$line;
	}
	print($line);
	$linecount++;
}
close(FILE);